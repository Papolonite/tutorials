extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (int) var max_extra_jump = 1
export (int) var extra_jump = 0

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
		extra_jump = 0
	if (not is_on_floor()) and (extra_jump != max_extra_jump) and Input.is_action_just_pressed('up'):
		extra_jump+= 1
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('right') and Input.is_mouse_button_pressed(1):
		velocity.x +=  1.5 * speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_pressed('left') and Input.is_mouse_button_pressed(1):
		velocity.x -= 1.5* speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
